from django.urls import path

from . import views

app_name = 'storydelapan'

urlpatterns = [
    path('', views.storydelapan, name='story8'),
    path('data', views.fungsi_buku, name='data'),
]
