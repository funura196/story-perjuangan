from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

def storydelapan(request):
    response = {}
    return render(request, 'storydelapan/booklist.html', response)

def fungsi_buku(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    result = requests.get(url)
    data = json.loads(result.content)
    return JsonResponse(data, safe=False)
    