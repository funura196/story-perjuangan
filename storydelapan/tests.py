from django.test import TestCase
from .views import storydelapan, fungsi_buku
from django.test import Client 
from django.urls import resolve, reverse
from .apps import StorydelapanConfig
from django.apps import apps

# Create your tests here.

class TestApps(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_story8_ada(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 404)
    
    def test_fungsi_data(self):
        response = Client().get('/story8/data?q=elsa')
        self.assertEqual(response.status_code, 404)
    

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.story8 = reverse("storydelapan:story8")

    def test_views_story8(self):
        response = self.client.get(self.story8)
        self.assertTemplateUsed(response, 'storydelapan/booklist.html')