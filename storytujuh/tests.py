from django.test import TestCase
from .views import storytujuh
from django.test import Client 
from django.urls import resolve, reverse
from .apps import StorytujuhConfig
from django.apps import apps

# Create your tests here.

class TestApps(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_story7_ada(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 404)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.story7 = reverse("storytujuh:storytujuh")

    def test_views_story7(self):
        response = self.client.get(self.story7)
        self.assertTemplateUsed(response, 'storytujuh/story7.html')
