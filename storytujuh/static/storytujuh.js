$( function() {
    $( "#accord-1", ).accordion({
        collapsible: true,
        active: false,
        heightStyle : "panel"
    });
    $( "#accord-2" ).accordion({
        collapsible: true,
        active: false,
        heightStyle : "panel"
    });
    $( "#accord-3" ).accordion({
        collapsible: true,
        active: false,
        heightStyle : "panel"
    });
    $( "#accord-4" ).accordion({
        collapsible: true,
        active: false,
        heightStyle : "panel"
    });
    $('.button-up').click(function () {
        var thisAccordion = $(this).parent().parent().parent();
        thisAccordion.insertBefore(thisAccordion.prev());
        event.stopPropagation(); 
        event.preventDefault(); 
    })

    $('.button-down').click(function () {
        var thisAccordion = $(this).parent().parent().parent();
        thisAccordion.insertAfter(thisAccordion.next());
        event.stopPropagation(); 
        event.preventDefault(); 
    })
});